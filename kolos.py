from flask import Flask,flash
from flask import render_template, request, jsonify
import string,operator,requests
#from celery import group
#import cellery_worker
import  os
import json
import couchdb

app = Flask(__name__)



app.secret_key='secret_key'

@app.route('/')
def hello_world():
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        server=couchdb.Server('http://194.29.175.241:5984')
        base=server['calc1']
        req=requests.get('http://194.29.175.241:5984/calc1/_design/utils/_view/list_active')
        parsed=json.loads(req.content)
        data=parsed['rows']

        znaleziono=False
        for all in data:
            if all['value']['host']=='http://stormy-earth-3453.herokuapp.com/':
                znaleziono=True
                id=all['id']
                print(id)
            else:
                znaleziono=False
                # szukanie operatora...

        if znaleziono==True:
            pass
            #document=json.dumps( {'host':'http://stormy-earth-3453.herokuapp.com/','active':True,'operator':'+','calculation':'/plus'})
            #base.save({'_id':id,'key':'+','value':{'host':'http://stormy-earth-3453.herokuapp.com/','active':True,'operator':'+','calculation':'/plus'}})
        if not znaleziono:

            document=json.dumps({'host':'http://stormy-earth-3453.herokuapp.com/','active':True,'operator':'+','calculation':'/plus'})
            requests.post('http://194.29.175.241:5984/calc1',data=document,headers=headers)
            print('l')
        print(parsed)

        return render_template("index.html")



@app.route('/calc_test', methods=['POST', 'GET'])
def calc_test():
    stack = []
    if request.method == "POST":
        operators = {
            "+": operator.add,
            "-": operator.sub,
            "*": operator.mul,
            "/": operator.div
            }
        expression =str(request.form['a'])
        elements = expression.split(" ")
        print(elements)
        pile = []
        try:
            while elements:
                e = elements.pop(0)
                print("lol")
                if e in operators:
                    b = pile.pop()
                    a = pile.pop()
                    pile.append(operators[e](a, b))
                else:
                    pile.append(int(e))
            result=json.dumps({'success':True,'result':float(pile[0])})

        except ValueError:
            result=json.dumps({'success':False,'result':"Wpisales niepoprawne dane badz wystapil inny blad"})

        print(result)
    return render_template("add_result.html",stack=result)

@app.route('/add_numbers', methods=['POST','GET'])
def add_numbers():
    try:
        a = int(request.form['a'])
        b =int(request.form['b'])
        result =a+b
    except ValueError:
        result="Please enter a correct number"

    return render_template("add_result.html",result=result)

@app.route('/server_list',methods=['GET','POST'])	
def server_list():
    req = requests.get('http://194.29.175.241:5984/calc1/_design/utils/_view/list_active')
    content=req.json()
    list = []
    for row in content['rows']:
        if row['value']['active']:
            list.append({'name': row['value']['host'], 'op': row['value']['operator'], 'calc' : row['value']['calculation']})
    ret=json.dumps(list)
    return render_template("add_result.html",stack=ret)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int(os.getenv('PORT', 5000)), debug=True)
